package com.techuniversity.catalogreview;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CatalogReviewApplication {

	public static void main(String[] args) {
		SpringApplication.run(CatalogReviewApplication.class, args);
	}

}
