package com.techuniversity.catalogreview.controllers;

import com.techuniversity.catalogreview.models.PersonModel;
import com.techuniversity.catalogreview.models.ServiceResponse;
import com.techuniversity.catalogreview.services.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v1")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE})
public class PersonController {
    @Autowired
    PersonService personService;

    @GetMapping("/persons")
    public ResponseEntity<List<PersonModel>> getPersons() {
        System.out.println("getPersons ");

        return new ResponseEntity<>(
                personService.findAll(),
                HttpStatus.OK
        );
    }

    @GetMapping("/persons/{id}")
    public ResponseEntity<Object> getPersonsById(@PathVariable String id) {
        System.out.println("getPersonsById");
        System.out.println("El id  a buscar es: " + id);

        Optional<PersonModel> result = personService.findById(id);

        return new ResponseEntity<>(
                result.isPresent() ? result : "No hay users con ese ID",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }

    @GetMapping(value = "/persons",params = {"name", "age"})
    public ResponseEntity<?> getPersonsByNameAge(@RequestParam(value = "name") String name, @RequestParam(value = "age") int age) {
        System.out.println("getPersonsByNameAge");
        System.out.println("El name de la person a buscar es: " + name);
        System.out.println("La edad de la person a buscar es: " + age);

        ServiceResponse result = personService.findByNameAndAge(name, age);
        return new ResponseEntity<>(result.getError() != null ? result.getError() : result.getMovie(), result.getCode());
    }

    @PostMapping("/persons")
    public ResponseEntity<PersonModel> addPerson(@RequestBody PersonModel person) {
        System.out.println("addPerson");
        System.out.println("La id de la person a crear es: " + person.getId());
        System.out.println("El name de la person a crear es: " + person.getName());
        System.out.println("La edad de la person a crear es: " + person.getAge());

        return new ResponseEntity<>(
                personService.add(person), HttpStatus.CREATED
        );
    }

    @PostMapping(path="/persons", consumes= MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> addAllPerson(@RequestBody List<PersonModel> listPerson) {
        System.out.println("addAllPerson");
        for (PersonModel person : listPerson) {
            System.out.println("La id de la person a crear es: " + person.getId());
            System.out.println("El name de la person a crear es: " + person.getName());
            System.out.println("La edad de la person a crear es: " + person.getAge());
        }
        ServiceResponse result = personService.addAll(listPerson);
        return new ResponseEntity<>(result.getError() != null ? result.getError() : result.getMovie(), result.getCode());

    }

    @PutMapping("/persons/{id}")
    public ResponseEntity<PersonModel> updatePerson(@RequestBody PersonModel person, @PathVariable String id) {
        System.out.println("updatePerson");
        System.out.println("La id del user de URL  es: " + id);
        System.out.println("El name del user a actualizar es: " + person.getName());
        System.out.println("La edad del user a actualizar es: " + person.getAge());

        Optional<PersonModel> userToUpdate = personService.findById(id);

        if (userToUpdate.isPresent()) {
            System.out.println("Persona  para actualizar encontrado, actualizando");
            person.setId(id);

            personService.update(person);
        }

        return new ResponseEntity<>(
                person,
                userToUpdate.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    @DeleteMapping("/persons/{id}")
    public ResponseEntity<String> deletePerson(@PathVariable String id) {
        System.out.println("deletePerson");
        System.out.println("La id de la persona a borrar es " + id);
        boolean deleteMovie = personService.delete(id);

        return new ResponseEntity<>(
                deleteMovie ? "Person deleted" : "Person not found",
                deleteMovie ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }
}
