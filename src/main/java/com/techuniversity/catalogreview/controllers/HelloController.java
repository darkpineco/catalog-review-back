package com.techuniversity.catalogreview.controllers;

import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE})
public class HelloController {

    @RequestMapping("/")
    public String index() {
        return "Hello, World!";
    }

    @RequestMapping("/hello")
    public String hello(@RequestParam(value = "name", defaultValue = "Tech U") String name) {
        return String.format("Hola %s!", name);
    }
	
	//testing access.
}
