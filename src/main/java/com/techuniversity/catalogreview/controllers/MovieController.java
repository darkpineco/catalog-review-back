package com.techuniversity.catalogreview.controllers;

import com.techuniversity.catalogreview.models.MovieModel;
import com.techuniversity.catalogreview.models.ServiceResponse;
import com.techuniversity.catalogreview.services.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v1")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE})
public class MovieController {

    @Autowired
    MovieService movieService;

    @GetMapping("/movies")
    public ResponseEntity<List<MovieModel>> getMovies(){
        System.out.println("getMovies");
        return new ResponseEntity<>(movieService.findAll(), HttpStatus.OK);
    }

    @GetMapping("/movies/{id}")
    public ResponseEntity<Object> getMoviesById(@PathVariable String id){
        System.out.println("Get Movie by id");
        System.out.println("La id de la pelicula a buscar es " + id);

        Optional<MovieModel> result = movieService.findById(id);

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "Movie not found",
                result.isPresent() ?  HttpStatus.OK :  HttpStatus.NOT_FOUND
        );
    }

    @PostMapping("/movies")
    public ResponseEntity<?> addMovie(@RequestBody MovieModel movie) {
        System.out.println("MovieController.addMovie: " + movie.toString());
        ServiceResponse result = movieService.add(movie);
        return new ResponseEntity<>(result.getError() != null ? result.getError() : result.getMovie(), result.getCode());
    }

    @PutMapping("/movies/{id}")
    public ResponseEntity<?> updateMovie(@PathVariable String id, @RequestBody MovieModel movie) {
        System.out.println("MovieController.addMovie: id: " + id + " movie: " + movie.toString());
        ServiceResponse result = movieService.update(id, movie);
        return new ResponseEntity<>(result.getError() != null ? result.getError() : result.getMovie(), result.getCode());
    }

    @DeleteMapping("/movies/{id}")
    public ResponseEntity<String> deleteMovie(@PathVariable String id) {
        System.out.println("deleteMovie");
        System.out.println("La id de la pelicula a borrar es " + id);
        boolean deleteMovie = movieService.delete(id);

        return new ResponseEntity<>(
                deleteMovie ? "Movie deleted" : "Movie not found",
                deleteMovie ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }
}
