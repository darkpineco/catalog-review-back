package com.techuniversity.catalogreview.services;

import com.techuniversity.catalogreview.models.PersonModel;
import com.techuniversity.catalogreview.models.ServiceError;
import com.techuniversity.catalogreview.models.ServiceResponse;
import com.techuniversity.catalogreview.repositories.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
public class PersonService {
    @Autowired
    PersonRepository personRepository;

    public List<PersonModel> findAll() {
        System.out.println("findAlL en PersonService");

        return personRepository.findAll();
    }

    public Optional<PersonModel> findById(String id) {
        System.out.println("findById en PersonService ");

        return personRepository.findById(id);
    }

    public Optional<PersonModel> findByName(String name) {
        System.out.println("findByName en PersonService ");

        return personRepository.findByName(name);
    }

    public PersonModel add(PersonModel person) {
        System.out.println("add en PersonService");

        return personRepository.save(person);
    }


    public ServiceResponse addAll(List<PersonModel> listPerson) {
        System.out.println("addAll en PersonService");
        ServiceResponse response = new ServiceResponse();

        if (listPerson != null && !listPerson.isEmpty()) {
            response.setCode(HttpStatus.CREATED);
            for (PersonModel person:personRepository.saveAll(listPerson)
                 ) {
                response.setPerson(person);
            }
        }else{
            ServiceError error = new ServiceError(HttpStatus.CONFLICT.value(), "No se ha creado el usario");
            response.setError(error);
            response.setCode(HttpStatus.CONFLICT);
        }

         return response ;
    }

    public ServiceResponse findByNameAndAge(String name, int age) {
        System.out.println("findByNameAge en PersonService");
        ServiceResponse response = new ServiceResponse();

        if (name != null && !name.isEmpty() && age > 0) {
            Optional<PersonModel> person = personRepository.findByNameAndAge(name, age);

            if (person.isPresent()) {
                response.setCode(HttpStatus.OK);
                response.setPerson(person.get());
            } else {
                ServiceError error = new ServiceError(HttpStatus.NOT_FOUND.value(), "No existe usuario con ese nombre y edad");
                response.setError(error);
                response.setCode(HttpStatus.CONFLICT);
            }
        } else {

            ServiceError error = new ServiceError(HttpStatus.BAD_REQUEST.value(), "Missing mandatory parameters");
            response.setError(error);
            response.setCode(HttpStatus.BAD_REQUEST);
        }
        return response;
    }

    public PersonModel update(PersonModel person) {
        System.out.println("update en PersonService");

        return personRepository.save(person);
    }

    public boolean delete (String id){
        System.out.println("delete en PersonService");
        boolean result = false;

        if (this.findById(id).isPresent()) {
            System.out.println("Person encontrada, borrando");

            this.personRepository.deleteById(id);
            result = true;
        }

        return result;
    }
}
