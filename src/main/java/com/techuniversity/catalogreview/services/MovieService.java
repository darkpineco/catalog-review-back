package com.techuniversity.catalogreview.services;

import com.techuniversity.catalogreview.models.MovieModel;
import com.techuniversity.catalogreview.models.PersonModel;
import com.techuniversity.catalogreview.models.ServiceError;
import com.techuniversity.catalogreview.models.ServiceResponse;
import com.techuniversity.catalogreview.repositories.MovieRepository;
import com.techuniversity.catalogreview.repositories.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class MovieService {

    @Autowired
    MovieRepository movieRepository;

    @Autowired
    PersonService personService;

    public List<MovieModel> findAll() {
        System.out.println("findAll en CatalogReviewService");
        return this.movieRepository.findAll();
    }

    public Optional<MovieModel> findById(String id) {
        System.out.println("MovieService.findById: " + id);
        return movieRepository.findById(id);
    }

    public ServiceResponse add(MovieModel movie) {
        System.out.println("MovieService.add: " + movie.toString());
        ServiceResponse response = new ServiceResponse();

        if (movie.getTitle() == null || movie.getTitle().isEmpty() || movie.getYear() == null || movie.getYear().isEmpty()) {
            ServiceError error = new ServiceError(HttpStatus.BAD_REQUEST.value(), "Missing mandatory parameters");
            response.setError(error);
            response.setCode(HttpStatus.BAD_REQUEST);
            return response;
        }

        if (movieRepository.findByTitleAndYear(movie.getTitle(), movie.getYear()).isPresent()) {
            ServiceError error = new ServiceError(HttpStatus.CONFLICT.value(), "Movie already exists");
            response.setError(error);
            response.setCode(HttpStatus.CONFLICT);
            return response;
        }

        processActors(movie);

        response.setCode(HttpStatus.OK);
        response.setMovie(movieRepository.insert(movie));
        return response;
    }

    public ServiceResponse update(String id, MovieModel movie) {
        System.out.println("MovieService.update: id: " + id + " movie: " + movie.toString());
        ServiceResponse response = new ServiceResponse();

        if (findById(id).isPresent()) {
            movie.setId(id);
            processActors(movie);
            response.setMovie(movieRepository.save(movie));
            response.setCode(HttpStatus.OK);
            return response;
        }

        response.setError(new ServiceError(HttpStatus.NOT_FOUND.value(), "Movie not found"));
        response.setCode(HttpStatus.NOT_FOUND);
        return response;
    }

    public boolean delete (String id){
        System.out.println("delete en CatalogReviewService");
        boolean result = false;

        if (this.findById(id).isPresent()) {
            System.out.println("Pelicula encontrada, borrando");

            this.movieRepository.deleteById(id);
            result = true;
        }

        return result;
    }

    private void processActors(MovieModel movie) {
        if (movie.getActors() != null && !movie.getActors().isEmpty()) {
            // check if actors exists and add to DB if not
            for (PersonModel actor : movie.getActors()) {
                Optional<PersonModel> foundActor = personService.findByName(actor.getName());
                if (foundActor.isPresent()) {
                    actor.setId(foundActor.get().getId());
                    actor.setName(foundActor.get().getName());
                    actor.setAge(foundActor.get().getAge());
                } else {
                    // This updates the current actor
                    personService.add(actor);
                }
            }
        }
    }
}
