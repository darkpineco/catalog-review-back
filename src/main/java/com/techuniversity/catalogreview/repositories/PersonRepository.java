package com.techuniversity.catalogreview.repositories;

import com.techuniversity.catalogreview.models.PersonModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PersonRepository extends MongoRepository<PersonModel, String> {

    Optional<PersonModel> findByNameAndAge(String name, int age);

    Optional<PersonModel> findByName(String name);

}
