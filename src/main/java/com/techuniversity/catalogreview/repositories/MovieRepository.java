package com.techuniversity.catalogreview.repositories;

import com.techuniversity.catalogreview.models.MovieModel;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface MovieRepository extends MongoRepository<MovieModel, String> {

    Optional<MovieModel> findByTitleAndYear(String title, String year);
}
