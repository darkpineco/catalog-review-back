package com.techuniversity.catalogreview.models;

import org.springframework.http.HttpStatus;

public class ServiceResponse {

    ServiceError error;
    MovieModel movie;
    PersonModel person;
    HttpStatus code;

    public ServiceResponse() {
    }

    public ServiceResponse(ServiceError error, MovieModel movie, PersonModel person, HttpStatus code) {
        this.error = error;
        this.movie = movie;
        this.person = person;
        this.code = code;
    }

    public ServiceError getError() {
        return error;
    }

    public void setError(ServiceError error) {
        this.error = error;
    }

    public MovieModel getMovie() {
        return movie;
    }

    public void setMovie(MovieModel movie) {
        this.movie = movie;
    }

    public HttpStatus getCode() {
        return code;
    }

    public void setCode(HttpStatus code) {
        this.code = code;
    }

    public PersonModel getPerson() {
        return person;
    }

    public void setPerson(PersonModel person) {
        this.person = person;
    }

    @Override
    public String toString() {
        return "ServiceResponse{" +
                "error=" + error +
                ", movie=" + movie +
                ", person=" + person +
                ", code=" + code +
                '}';
    }
}
