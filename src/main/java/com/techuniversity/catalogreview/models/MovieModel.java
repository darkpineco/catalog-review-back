package com.techuniversity.catalogreview.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document(collection = "movies")  // Collection = Table; @Entity = @Document
public class MovieModel {

    @Id
    private String id;
    private String title;
    private String year;
    private String synopsis;
    private String genre;
    private List<PersonModel> actors;
    private String image;

    public MovieModel() {
    }

    public MovieModel(String id, String title, String year, String synopsis, String genre, String image, List<PersonModel> actors) {
        this.id = id;
        this.title = title;
        this.year = year;
        this.synopsis = synopsis;
        this.genre = genre;
        this.image = image;
        this.actors = actors;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getSynopsis() {
        return synopsis;
    }

    public void setSynopsis(String synopsis) {
        this.synopsis = synopsis;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public List<PersonModel> getActors() {
        return actors;
    }

    public void setActors(List<PersonModel> actors) {
        this.actors = actors;
    }

    @Override
    public String toString() {
        return "MovieModel{" +
                "id='" + id + '\'' +
                ", title='" + title + '\'' +
                ", year='" + year + '\'' +
                ", synopsis='" + synopsis + '\'' +
                ", genre='" + genre + '\'' +
                ", image='" + image + '\'' +
                ", actors=" + actors +
                '}';
    }
}
